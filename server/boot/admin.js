module.exports = function(app) {
  var User = app.models.User;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;

  // If we see these environment variables we'll create an Admin user.
  // Otherwise we assume there's nothing to do.  
  if (process.env.store_user && process.env.store_email && process.env.store_password) {

    //make an admin, only if one does not exist
    User.findOrCreate( {username: process.env.store_user}, {
      username: process.env.store_user,
      email: process.env.store_email,
      password: process.env.store_password
    }
    , function(err, user) {
      if (err) throw err;
        Role.findOrCreate({name: 'admin'}, {
        name: 'admin'
      }, function(err, role) {
        if (err) throw err;
        // There doesn't seem to be a findOrCreate() for principals?
        role.principals.find( {principalId: user.id},
          function(err, principal) {
            if (err) throw err;
              if (principal.length < 1) role.principals.create({
                principalType: RoleMapping.USER,
                principalId: user.id
              }, function (err, principal){
                if (err) throw err;
              });
        });
      });
    
    });
  };
}
